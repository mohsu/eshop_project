package com.sun.dubbo.webmanage.service;

import com.sun.webmanage.model.TbOrder;

public interface TbOrderDubboService {

	boolean insertTbOrder(TbOrder tbOrder);
	
	boolean updateTbOrder(TbOrder tbOrder);
	
}
